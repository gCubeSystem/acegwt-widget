# Changelog



## [v1.1.1] [r4.23.0] - 2020-05-20

### Fixes

- Updated GitHub client library [#18318]



## [v1.1.0] - 2017-06-12

### Features

- Support Java 8 compatibility [#8541]



## [v1.0.0] - 2016-03-16

- First Release



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
